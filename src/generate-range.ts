// generateRange(2, 10, 2) - should return array of [2,4,6,8,10]
// generateRange(1, 10, 3) - should return array of [1,4,7,10]


// function generateRange(min: number, max: number, step: number){
//     let result = [];
//     for(let i = min; i <= max; i = i + step) {
//         result.push(i);
//     }
//     return result;
// }

// console.log('generateRange', generateRange(1,10,3))


// concatValues([min],ofValue)
import { pipe } from 'fp-ts/lib/pipeable'
import { Either } from 'fp-ts/lib/Either'

const findNextStep = (value: number) => (step: number) => value + step
const checkValue = (max: number, curr: number): boolean => curr <= max
const ofValue = (current: number): Array<number> => [current]
const concatValues = (prev: Array<number>) => (curr: Array<number>) => prev.concat(curr)
// const getValue = (max: number) => (curr: number): Either<Array<number>> => checkValue(max, curr) ? ofValue(curr) : []
// const getValue = (max: number) => (curr: number): Array<number> => checkValue(max, curr) ? ofValue(curr).concat(getValue(max)(curr)) : ofValue(curr)
// [1].


const box = (x: number) => ({
  value: x,
  concat: (f: Function) => box(f(x))
  
})
const generateRange = (min:number) => (max:number) => (step:number): Array<number> => {
  const logPipe = (value: any) => {
    console.log(value)
    return value
  }
  const result = (acc: Array<number>): Array<number> => pipe(
    findNextStep(acc[acc.length - 1])(step), //number
    logPipe,
    // getValue(max), // Array<number>
    concatValues(acc), // Array<number>
    result
  )
  return result([min])
}


console.log('generateRange', generateRange(2)(10)(2))