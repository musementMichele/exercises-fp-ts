"use strict";
// generateRange(2, 10, 2) - should return array of [2,4,6,8,10]
// generateRange(1, 10, 3) - should return array of [1,4,7,10]
// function generateRange(min: number, max: number, step: number){
//     let result = [];
//     for(let i = min; i <= max; i = i + step) {
//         result.push(i);
//     }
//     return result;
// }
// console.log('generateRange', generateRange(1,10,3))
var min = 1;
var max = 10;
var step = 3;
var findNextStep = function (value) { return function (step) { return value + step; }; };
var checkValue = function (max) { return function (curr) { return curr <= max; }; };
var Right = function (x) { return ({
    ap: function (box2) { return box2.map(x); },
    map: function (f) { return Right(f(x)); },
    fold: function (_, g) { return g(x); },
    get: function () { return x; }
}); };
var Left = function (x) { return ({
    map: function (f) { return Left(x); },
    fold: function (f, _) { return f(x); },
    get: function () { return x; }
}); };
var liftA2 = function (f, fx, fy) {
    return fx.map(f).ap(fy);
};
var legit = function (x) {
    return x.get() ? Right(x) : Left(null);
};
var result = [min];
var accumulator = function (elem) {
    result.push(elem);
};
var loop = function (max) { return function (step) { return function generateRange(current) {
    var val = liftA2(findNextStep, Right(current), Right(step));
    var result = liftA2(checkValue, Right(max), Right(val.get()));
    var keep = legit(result);
    keep.fold(function (err) { }, function (success) { accumulator(val.get()); generateRange(val.get()); });
}; }; };
loop(max)(step)(min);
console.log("generate range 2", result);
