"use strict";
// generateRange(2, 10, 2) - should return array of [2,4,6,8,10]
// generateRange(1, 10, 3) - should return array of [1,4,7,10]
exports.__esModule = true;
// function generateRange(min: number, max: number, step: number){
//     let result = [];
//     for(let i = min; i <= max; i = i + step) {
//         result.push(i);
//     }
//     return result;
// }
// console.log('generateRange', generateRange(1,10,3))
// concatValues([min],ofValue)
var pipeable_1 = require("fp-ts/lib/pipeable");
var findNextStep = function (value) { return function (step) { return value + step; }; };
var checkValue = function (max, curr) { return curr <= max; };
var ofValue = function (current) { return [current]; };
var concatValues = function (prev) { return function (curr) { return prev.concat(curr); }; };
// const getValue = (max: number) => (curr: number): Either<Array<number>> => checkValue(max, curr) ? ofValue(curr) : []
// const getValue = (max: number) => (curr: number): Array<number> => checkValue(max, curr) ? ofValue(curr).concat(getValue(max)(curr)) : ofValue(curr)
// [1].
var box = function (x) { return ({
    value: x,
    concat: function (f) { return box(f(x)); }
}); };
var generateRange = function (min) { return function (max) { return function (step) {
    var logPipe = function (value) {
        console.log(value);
        return value;
    };
    var result = function (acc) { return pipeable_1.pipe(findNextStep(acc[acc.length - 1])(step), //number
    logPipe, 
    // getValue(max), // Array<number>
    concatValues(acc), // Array<number>
    result); };
    return result([min]);
}; }; };
console.log('generateRange', generateRange(2)(10)(2));
