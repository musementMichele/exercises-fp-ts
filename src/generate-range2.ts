// generateRange(2, 10, 2) - should return array of [2,4,6,8,10]
// generateRange(1, 10, 3) - should return array of [1,4,7,10]


// function generateRange(min: number, max: number, step: number){
//     let result = [];
//     for(let i = min; i <= max; i = i + step) {
//         result.push(i);
//     }
//     return result;
// }

// console.log('generateRange', generateRange(1,10,3))

const min = 1;
const max = 10;
const step = 3;

const findNextStep = (value: number) => (step: number) => value + step
const checkValue = (max: number)=> (curr: number): boolean => curr <= max

const Right = (x: any) => ({
  ap: (box2: any) => box2.map(x),
  map: (f: Function) => Right(f(x)),
  fold: (_: Function, g: Function) => g(x),
  get: () => x,
})

const Left = (x: any) =>({
  map: (f: Function) => Left(x),
  fold: (f: Function, _: Function) => f(x),
  get: () => x
})

const liftA2 = (f: any, fx: any, fy: any) =>
    fx.map(f).ap(fy)

const legit = (x: any) =>
  x.get() ? Right(x) : Left(null)

let result: any = [min];
const accumulator = (elem: any) => {
    result.push(elem);
}

const loop = (max: any) => (step: any) => function generateRange(current: any) {
    const val = liftA2(findNextStep, Right(current), Right(step))
    const result = liftA2(checkValue, Right(max), Right(val.get()));
    const keep = legit(result);
    keep.fold((err: any) => {}, (success: any) => {accumulator(val.get()); generateRange(val.get())})
}

loop(max)(step)(min)
console.log("generate range 2",result);